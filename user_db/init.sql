CREATE DATABASE users;
use users;

CREATE TABLE users (
  id INTEGER NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  password_hash VARCHAR(128) NOT  NUll,
  PRIMARY KEY(id)
);



/*
insert into users (first_name, last_name, email, password_hash ) values ('Albert', 'Sturman', 'asturman1@unesco.org', 'IrQiFnjCO3JR');
insert into users (first_name, last_name, email, password_hash ) values ('Aldus', 'Crowcroft', 'acrowcroft2@slate.com', 'ENnYYAGlCsD');
insert into users (first_name, last_name, email, password_hash ) values ('Marjy', 'Elcock', 'melcock3@ehow.com', 'mJxzHNWIEm5Y');
insert into users (first_name, last_name, email, password_hash ) values ('Vernon', 'Heskin', 'vheskin4@w3.org', 'veLwCY');
insert into users (first_name, last_name, email, password_hash ) values ('Raynor', 'Winskill', 'rwinskill5@liveinternet.ru', 'DuFomQ');
insert into users (first_name, last_name, email, password_hash ) values ('Odie', 'Casaletto', 'ocasaletto6@europa.eu', 'zBCDx8zR');
insert into users (first_name, last_name, email, password_hash ) values ('Kissiah', 'Camus', 'kcamus7@ft.com', 'e7t6YUo6');
insert into users (first_name, last_name, email, password_hash ) values ('Melisent', 'Salmond', 'msalmond8@people.com.cn', 'PEMpTK50im');
insert into users (first_name, last_name, email, password_hash ) values ('Vale', 'Pawel', 'vpawel9@army.mil', 'hjk9pR');
insert into users (first_name, last_name, email, password_hash ) values ('Camel', 'O''Reilly', 'coreillya@yelp.com', 'gHwnjz');
insert into users (first_name, last_name, email, password_hash ) values ('Dominic', 'Kedie', 'dkedieb@vk.com', '1ZFjrE3UDZ');
insert into users (first_name, last_name, email, password_hash ) values ('Arielle', 'Brickner', 'abricknerc@hexun.com', 'hXdIQsISNQ');
insert into users (first_name, last_name, email, password_hash ) values ('Dionne', 'Lester', 'dlesterd@ehow.com', 'vxghNe');
insert into users (first_name, last_name, email, password_hash ) values ('Claire', 'Springer', 'cspringere@census.gov', 'YfML4a');
insert into users (first_name, last_name, email, password_hash ) values ('Shantee', 'Northage', 'snorthagef@cocolog-nifty.com', 'secbKuF');
insert into users (first_name, last_name, email, password_hash ) values ('Reginauld', 'Healey', 'rhealeyg@mac.com', 'xLp3wh');
insert into users (first_name, last_name, email, password_hash ) values ('Gustaf', 'd''Escoffier', 'gdescoffierh@ameblo.jp', 'ZuC7o0S');
insert into users (first_name, last_name, email, password_hash ) values ('Emelia', 'Teresi', 'eteresii@yahoo.co.jp', 'PTW0VbI');
insert into users (first_name, last_name, email, password_hash ) values ('Herold', 'Gyngell', 'hgyngellj@apache.org', 'kSkpslP4msoh');
insert into users (first_name, last_name, email, password_hash ) values ('Zed', 'Tadlow', 'ztadlowk@drupal.org', 'mZSCw6o');
insert into users (first_name, last_name, email, password_hash ) values ('Alexandr', 'Hellwig', 'ahellwigl@paypal.com', 'NK6RFgW');
insert into users (first_name, last_name, email, password_hash ) values ('Symon', 'Woakes', 'swoakesm@spotify.com', 'WisHP2rr44M');
insert into users (first_name, last_name, email, password_hash ) values ('Collie', 'Rubenovic', 'crubenovicn@addthis.com', 'hBvLyOJn6b');
insert into users (first_name, last_name, email, password_hash ) values ('Lay', 'Norcop', 'lnorcopo@nasa.gov', '5UpTCZbREi');
insert into users (first_name, last_name, email, password_hash ) values ('Joachim', 'Bayston', 'jbaystonp@ask.com', 'o86QXDNiE');
insert into users (first_name, last_name, email, password_hash ) values ('Erskine', 'Risbie', 'erisbieq@sphinn.com', 'uVUgcb');
insert into users (first_name, last_name, email, password_hash ) values ('Shell', 'Sibthorp', 'ssibthorpr@upenn.edu', 'jVZr7sK');
insert into users (first_name, last_name, email, password_hash ) values ('Chrysa', 'Macer', 'cmacers@foxnews.com', 'o4HAt3');
insert into users (first_name, last_name, email, password_hash ) values ('Rona', 'Castagnet', 'rcastagnett@yale.edu', 'eXQWKEnv');
insert into users (first_name, last_name, email, password_hash ) values ('Lamond', 'Quinnette', 'lquinnetteu@i2i.jp', 'KJEkRQOh');
insert into users (first_name, last_name, email, password_hash ) values ('Alix', 'Berzen', 'aberzenv@multiply.com', 'iU2jqNr');
insert into users (first_name, last_name, email, password_hash ) values ('Penelopa', 'Perocci', 'pperocciw@theatlantic.com', 'LtAAI0Hb');
insert into users (first_name, last_name, email, password_hash ) values ('Letisha', 'Patrickson', 'lpatricksonx@csmonitor.com', '4t8DTR5S0');
insert into users (first_name, last_name, email, password_hash ) values ('Milty', 'McKleod', 'mmckleody@cocolog-nifty.com', 'bU1WcEZ');
insert into users (first_name, last_name, email, password_hash ) values ('Kelly', 'Sattin', 'ksattinz@mlb.com', '2g85R76iO');
insert into users (first_name, last_name, email, password_hash ) values ('Shep', 'Trimble', 'strimble10@mit.edu', 'OPi6kK');
insert into users (first_name, last_name, email, password_hash ) values ('Jeromy', 'Thredder', 'jthredder11@admin.ch', 'll2Vitto');
insert into users (first_name, last_name, email, password_hash ) values ('Pepi', 'Panner', 'ppanner12@aol.com', 'mkguip5zxeO');
insert into users (first_name, last_name, email, password_hash ) values ('Rhonda', 'Jann', 'rjann13@army.mil', 'UPLzMO');
insert into users (first_name, last_name, email, password_hash ) values ('Kendricks', 'Bonnefin', 'kbonnefin14@zimbio.com', 'KTLkP1EysRp');
insert into users (first_name, last_name, email, password_hash ) values ('Sylvia', 'Colam', 'scolam15@addthis.com', 'JhLNa2E');
insert into users (first_name, last_name, email, password_hash ) values ('Ferris', 'Cockayne', 'fcockayne16@tamu.edu', 'uBtffWp');
insert into users (first_name, last_name, email, password_hash ) values ('Cybil', 'Wallentin', 'cwallentin17@noaa.gov', 'DbyyM8z');
insert into users (first_name, last_name, email, password_hash ) values ('Josi', 'Zamorano', 'jzamorano18@meetup.com', 'KmXTskt');
insert into users (first_name, last_name, email, password_hash ) values ('Rebecca', 'Staddon', 'rstaddon19@blogger.com', 'ocPbt3');
insert into users (first_name, last_name, email, password_hash ) values ('Eimile', 'Searchfield', 'esearchfield1a@unicef.org', 'tUV3lcMf');
insert into users (first_name, last_name, email, password_hash ) values ('Torrence', 'Myrkus', 'tmyrkus1b@networksolutions.com', 'JOCn4lP8');
insert into users (first_name, last_name, email, password_hash ) values ('Scarlet', 'Bredgeland', 'sbredgeland1c@guardian.co.uk', 'mPFxX81r3w');
insert into users (first_name, last_name, email, password_hash ) values ('Nickolaus', 'Duncklee', 'nduncklee1d@toplist.cz', 'QyPb3I');
*/



