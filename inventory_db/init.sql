CREATE DATABASE inventory;
use inventory;

CREATE TABLE items (
  itemID INTEGER NOT NULL AUTO_INCREMENT,
  item_name VARCHAR (200) NOT NUll,
  item_description VARCHAR(200) NOT NULL,
  item_price VARCHAR(20) NOT NULL,
  PRIMARY KEY(itemID)
);

INSERT INTO items (item_name, item_description, item_price) values ('Jurassic World ','Movie Deal 1: 2 x Adults , 2 x Children' , '26.10');
insert into items (item_name, item_description, item_price) values ('Jurassic World ','Movie Deal 2: 3 x Adults ', '23.36');
insert into items (item_name, item_description, item_price) values ('Jurassic World ','Movie Deal 3: 5 x concession ', '18.60');
insert into items (item_name, item_description, item_price) values ('Jurassic World ','Movie Deal 4: 1 x Adults, 2 x Children ','15');
insert into items (item_name, item_description, item_price) values ('Jurassic World ','Movie Deal 5: 2 x seniors', '10');
INSERT INTO items (item_name, item_description, item_price) values ('Avengers ','Movie Deal 1: 2 x Adults , 2 x Children' , '26.10');
insert into items (item_name, item_description, item_price) values ('Avengers  ','Movie Deal 2: 3 x Adults ', '23.36');
insert into items (item_name, item_description, item_price) values ('Avengers ','Movie Deal 3: 5 x concession ', '18.60');
insert into items (item_name, item_description, item_price) values ('Avengers  ','Movie Deal 4: 1 x Adults, 2 x Children ','15');
insert into items (item_name, item_description, item_price) values ('Avengers ','Movie Deal 5: 2 x seniors', '10');
INSERT INTO items (item_name, item_description, item_price) values ('Star Wars ','Movie Deal 1: 2 x Adults , 2 x Children' , '26.10');
insert into items (item_name, item_description, item_price) values ('Star Wars  ','Movie Deal 2: 3 x Adults ', '23.36');
insert into items (item_name, item_description, item_price) values ('Star Wars ','Movie Deal 3: 5 x concession ', '18.60');
insert into items (item_name, item_description, item_price) values ('Star Wars  ','Movie Deal 4: 1 x Adults, 2 x Children ','15');
insert into items (item_name, item_description, item_price) values ('Star Wars  ','Movie Deal 5: 2 x seniors', '10');
INSERT INTO items (item_name, item_description, item_price) values ('Coco','Movie Deal 1: 2 x Adults , 2 x Children' , '26.10');
insert into items (item_name, item_description, item_price) values ('Coco ','Movie Deal 2: 3 x Adults ', '23.36');
insert into items (item_name, item_description, item_price) values ('Coco','Movie Deal 3: 5 x concession ', '18.60');
insert into items (item_name, item_description, item_price) values ('Coco','Movie Deal 4: 1 x Adults, 2 x Children ','15');
insert into items (item_name, item_description, item_price) values ('Coco','Movie Deal 5: 2 x seniors', '10');
INSERT INTO items (item_name, item_description, item_price) values ('A Quiet Place ','Movie Deal 1: 2 x Adults , 2 x Children' , '26.10');
insert into items (item_name, item_description, item_price) values ('A Quiet Place ','Movie Deal 2: 3 x Adults ', '23.36');
insert into items (item_name, item_description, item_price) values ('A Quiet Place','Movie Deal 3: 5 x concession ', '18.60');
insert into items (item_name, item_description, item_price) values ('A Quiet Place ','Movie Deal 4: 1 x Adults, 2 x Children ','15');
insert into items (item_name, item_description, item_price) values ('A Quiet Place','Movie Deal 5: 2 x seniors', '10');
INSERT INTO items (item_name, item_description, item_price) values ('The Greatest Showman','Movie Deal 1: 2 x Adults , 2 x Children' , '26.10');
insert into items (item_name, item_description, item_price) values ('The Greatest Showman','Movie Deal 2: 3 x Adults ', '23.36');
insert into items (item_name, item_description, item_price) values ('The Greatest Showman','Movie Deal 3: 5 x concession ', '18.60');
insert into items (item_name, item_description, item_price) values ('The Greatest Showman','Movie Deal 4: 1 x Adults, 2 x Children ','15');
insert into items (item_name, item_description, item_price) values ('The Greatest Showman','Movie Deal 5: 2 x seniors', '10');